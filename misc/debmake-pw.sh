#!/bin/bash

export DEBFULLNAME="Dashamir Hoxha"
export DEBEMAIL="dashohoxha@gmail.com"
debmake -b':sh' \
    --tar --package pw --upstreamversion 1.2 \
    --email dashohoxha@gmail.com --fullname "Dashamir Hoxha"
