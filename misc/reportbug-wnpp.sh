#!/bin/bash
reportbug \
    -s "ITP: pw -- A simple command-line password manager" \
    -S wishlist \
    --list-cc=debian-devel@lists.debian.org \
    --body-file=wnpp-pw.txt \
    -O --mutt wnpp
