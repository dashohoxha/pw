#!/bin/bash
reportbug \
    -s "RFS: pw/1.2-1 [ITP] -- simple command-line password manager" \
    -S normal \
    --list-cc=debian-devel@lists.debian.org \
    --body-file=rfs.txt \
    -O --mutt sponsorship-requests
